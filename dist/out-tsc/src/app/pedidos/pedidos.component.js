"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var api_service_1 = require("../api.service");
var PedidosComponent = /** @class */ (function () {
    function PedidosComponent(apiService) {
        this.apiService = apiService;
        this.pedido = {};
        this.pizzas = [];
        this.bebidas = [];
    }
    PedidosComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.apiService.getPizza().subscribe(function (data) {
            _this.pizzas = data;
        });
        this.apiService.getBebida().subscribe(function (data) {
            _this.bebidas = data;
        });
    };
    PedidosComponent = __decorate([
        core_1.Component({
            selector: 'app-pedidos',
            templateUrl: './pedidos.component.html',
            styleUrls: ['./pedidos.component.css']
        }),
        __metadata("design:paramtypes", [api_service_1.ApiService])
    ], PedidosComponent);
    return PedidosComponent;
}());
exports.PedidosComponent = PedidosComponent;
//# sourceMappingURL=pedidos.component.js.map